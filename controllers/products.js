// [SECTION] Dependencies and Modules
	const Prod = require('../models/Product');

// [SECTION] Functionalities [POST]
	// Create Product
	module.exports.createProd = (data) => {
		let pName = data.name;
		let pDesc = data.description;
		let pPrice = data.price;

		let newProd = new Prod({
			name: pName,
			description: pDesc,
			price: pPrice
		});

		return newProd.save().then((prod, err) => {
			if (prod) {
				return `Product "${prod.name}", was successfully added to your Product List!
				Please see below details:
				${prod}`;
			} else {
				return `There was an error in registering the product.`
			}
		});
	};


	// [SECTION] Functionality [Delete]
	module.exports.deleteProduct = (productId) => {
		return Prod.findByIdAndRemove(productId).then((removedProduct, err) => {
			if (removedProduct) {
				return 'Product successfully removed';
			} else {
				return 'No Product was Removed';
			};
		});
	};



	// Retrieve Single Product
	module.exports.getProd = (id) => { 
   		return Prod.findById(id).then(result => {
   			return result;
   		});
   };

// [SECTION] Functionalities [GET]
	// Retrieve ALL Active Products
	module.exports.getAllProd = () => {
		return Prod.find({}).then(result => {
			return `Welcome to our Products Listings. Please see below to view the details:
			${result}`;
		});
	};

	// Retrieve ALL Active Products
	module.exports.getAllActiveProd = () => {
		return Prod.find({isActive: true}).then(result => {
			return `Welcome to our Products Listings. Please see below to view the details:
			${result}`;
		});
	};

	// Retrieve ALL Inactive Products
	module.exports.getAllInactiveProd = () => {
		return Prod.find({isActive: false}).then(result => {
			return `Welcome to our Products Listings. Please see below to view the details:
			${result}`;
		});
	};


// [SECTION] Functionalities [PUT]
	// Update Product Details
	module.exports.updateProd = (prod, details) => {
		let pName = details.name;
		let pDesc = details.description;
		let pCost = details.price
		let updatedProd = {
			name: pName,
			description: pDesc,
			price: pCost
		};
		let id = prod.prodId;
		return Prod.findByIdAndUpdate(id, updatedProd).then((updated, err) => {
			if (updated) {
				return `Successfully updated product information!`;
			} else {
				return `Failed to update product information.`;
			}
		});
	};

	// Product Archive
	module.exports.archiveProd = (prod) => {
		let id = prod.prodId;
		let updates = {isActive: false};
		return Prod.findByIdAndUpdate(id, updates).then((archived, error) => {
			if (archived) {
				return	`Product ${archived.name} was archived. Restock to re-enable it back to the list.`;
			} else {
				return 	`Failed to archive the product`;
			}
		})
	};

