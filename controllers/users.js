// [SECTION] Dependencies and Modules
   const User = require('../models/User');
   const bcrypt = require('bcrypt');
   const auth = require('../auth');

// [SECTION] Funtionalities [Create]
   // Register User
   module.exports.registerUser = (data) => {
      let fName = data.firstName;
      let lName = data.lastName;
      let mName = data.middleName;
      let email = data.email;
      let passW = data.password;

      let newUser = new User({
         firstName: fName,
         lastName: lName,
         middleName: mName,
         email: email,
         password: bcrypt.hashSync(passW, 10)
      });

      return newUser.save().then((user, err) => {
         if (user) {
            return `Successfully created an account for ${fName +' '+ lName}. Please see the following details:
            ${user}`;
         } else {
            return `There was an error in saving your account to the database.`;
         }
      });
   };

// [SECTION] Functionalities [LOGIN]
   // Login (User Authentication)
   module.exports.loginUser = (reqBody) => {
      let uEmail = reqBody.email;
      let uPass = reqBody.password;
      return User.findOne({email: uEmail}).then(result => {
         if (result === null) {
            return `Email does not exist!`;
         } else {
         let passW = result.password;
            const isMatched = bcrypt.compareSync(uPass, passW);
            if (isMatched) {
               let data = result.toObject();
               return {access: auth.createAccessToken(data)}
            } else {
               return `Passwords does not match. Check credentials.....`
            }
         }
      });
   };

// [SECTION] Functionalities [Change Password]
   // Change Password
   module.exports.updatePass = (reqBody) => {
      let uEmail = reqBody.email;
      let uPass = reqBody.password;
      let nPass = reqBody.newpassword;

      return User.findOne({email: uEmail}).then(result => {
         if (result === null) {
            return `Email does not exist!`;
         } else {
         let passW = result.password;
         let isMatched = bcrypt.compareSync(uPass, passW);
            if (isMatched) {
               let dataNiUser = result.toObject();
               let updates = {
                  password: bcrypt.hashSync(nPass, 10)
                  }
               return User.findByIdAndUpdate(dataNiUser, updates).then((user, err) => {
                  if (user) {
                     return `Successfully updated the password!`;
                  } else {
                     return `Unable to update user information.`;
                  };
               });
            } else {
               return `Passwords does not match. Check credentials.`
            };
         };
      });
   };




// [SECTION] Functionalities [PUT]
   // Update Product Details
   module.exports.updateProd = (prod, details) => {
      let pName = details.name;
      let pDesc = details.description;
      let pCost = details.price
      let updatedProd = {
         name: pName,
         description: pDesc,
         price: pCost
      };
      let id = prod.prodId;
      return Prod.findByIdAndUpdate(id, updatedProd).then((updated, err) => {
         if (updated) {
            return `Successfully updated product information!`;
         } else {
            return `Failed to update product information.`;
         }
      });
   };

// [SECTION] Functionality [Retrieve]
   // Retrieve all courses (Admin)
   module.exports.getAllUsers = () => {
      return User.find({}).then(result => {
         return result;
      });
   };

   // Retrieve Authenticated User's Orders
   module.exports.getUserOrd = (id) => {
      return User.findById(id).then(user => {
         return user.orders;
      });
   };

// [SECTION] Functionalities [Update]
   // Set User as Admin
   module.exports.setAdmin = (userId) => {
      let updates = {
         isAdmin: true
      }
      return User.findByIdAndUpdate(userId, updates).then((admin, err) => {
         if (admin) {
            return `Successfully updated ${admin.firstName} to Admin!`;
         } else {
            return `Updates failed to implement.....`;
         };
      });
   };

   // Set User as Non-Admin
   module.exports.setNonAdmin = (userId) => {
      let updates = {
         isAdmin: false
      }
      return User.findByIdAndUpdate(userId, updates).then((user, err) => {
         if (user) {
            return `Successfully updated ${user.firstName} to Non-Admin`;
         } else {
            return `Unable to update user information....`;
         };
      });
   };

