// [SECTION] Dependencies and Modules
	const Order = require('../models/Order');
	const Prod = require('../models/Product');
	const User = require('../models/User');
	const auth = require('../auth');

// [SECTION] Funtionalities [Create]
	// Create Order
	module.exports.createOrder = async (data) => {
		let pId = data.prodId;
		let qty = data.quantity;
		let uId = data.userId;

		let cost = await Prod.findById(pId).then(result => result.price * qty);

		let userUp = await User.findById(uId).then(user => {
			user.orders.push({productId: pId});
			return user.save().then((save, err) => {
				if (err) {
					return false;
				} else {
					return true;
				}
			});
		});

		let prodUp = await Prod.findById(pId).then(prod => {
			prod.orders.push({userId: uId});
			return prod.save().then((save, err) => {
				if (err) {
					return false;
				} else {
					return true;
				}
			});
		});

		let newOrder = new Order({
			prodId: pId,
			quantity: qty,
			totalAmt: cost,
			userId: uId
		});

		return newOrder.save().then((save, err) => {
			if (err) {
				return `There was an error in processing your order to the database.`;
			} else {
				return save;
			}
		});
	};

// [SECTION] Functionalities [GET]
	// Retrieve ALL Orders
	module.exports.getAllOrders = () => {
		return Order.find({}).then(result => {
			return result;
		});
	};

// [SECTION] Functionality [Delete]
	module.exports.deleteOrder = (orderId) => {
		return Order.findById(orderId).then(order => {
			if (order === null) {
				return `No resource was found!`;
			} else {
				return order.remove().then( (removedorder, err) =>{
					if (err) {
						return `Failed to remove order....`;
					} else {
						return `Successfully removed the order!`;
					};
				});
			};
		});
	};

// [SECTION] Functionalities [GET]
	// Checkout ALL Orders
	module.exports.checkAllOrders =(id) => {
		return Order.find({userId: id}).then(result => {
			
			return result;
		});
	};