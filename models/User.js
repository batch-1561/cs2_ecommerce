// [SECTION] Dependencies and Modules
	const mongo = require('mongoose');

// [SECTION] Schema
	const userSchema = new mongo.Schema({
		firstName: {
			type: String,
			required: [true, 'First Name is Required!']
		},
		lastName: {
			type: String,
			required: [true, 'Last Name is Required!']
		},
		middleName: {
			type: String,
			required: [true, 'Middle Name is Required!']
		},
		email: {
			type: String,
			required: [true, 'Email is Required!']
		},
		password: {
			type: String,
			required: [true, 'Password is Required!']
		},
		isAdmin: {
			type: Boolean,
			default: false 
		},
		orders:[{
			productId: {
				type: String,
				required: [true, 'Product ID is Required!']
			}
		}]
	});

// [SECTION] Models
	module.exports = mongo.model("User", userSchema);
