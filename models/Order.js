// [SECTION] Dependencies and Modules
	const mongo = require('mongoose');

// [SECTION] Schema
	const orderSchema = new mongo.Schema({
		totalAmt: {
			type: Number, 
			required: [true, 'Total Amount is Required!']
		},
		purchasedOn: {
			type: Date, 
			default: new Date()
		},
		quantity: {
			type: Number, 
			required: [true, 'Quantity is Required!']
		},
		prodId: {
			type: String, 
			required: [true, 'Product Name is Required!']
		},
		userId: {
			type: String,
			required: [true, 'User ID is Required!']
		},
	});

// [SECTION] Models
	module.exports = mongo.model("Order", orderSchema);
