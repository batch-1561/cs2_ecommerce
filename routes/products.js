// [SECTION] Dependencies and Modules
  const exp = require('express');
  const controller = require('../controllers/products');
  const auth = require('../auth');

// [SECTION] Routing Component
  const route = exp.Router();

// [SECTION] Route [POST]
  route.post('/create',auth.verify, (req, res) => {
    let token = req.headers.authorization;
    let isAdmin = auth.decode(token).isAdmin;
    let prodData = req.params.productId;
    isAdmin ? controller.createProd(prodData).then(outcome => res.send(outcome)
      )
    :
    res.send('Unauthorized');
  });

  // [SECTION] Route [DEL]
  //[SECTION]-[DEL] Route Administrator Only
   route.delete('/:productId', (req, res) => {
    let token = req.headers.authorization;
    let isAdmin = auth.decode(token).isAdmin;
    let id = req.params.productId;
    isAdmin ? controller.deleteProduct(id).then(outcome => res.send(outcome)
     )
    :
    res.send('Unauthorized');
  }); 


// [SECTION] Route [GET]
  // Retrieve All Products
  route.get('/all', (req, res) => {
    let token = req.headers.authorization;
    let isAdmin = auth.decode(token).isAdmin;
    isAdmin ? controller.getAllProd().then(outcome => res.send(outcome)
      )
    :
    res.send('Unauthorized');
  });

   // Retrieve All Active Products
  route.get('/active', (req, res) => {
    let token = req.headers.authorization;
    let isAdmin = auth.decode(token).isAdmin;
    isAdmin ? controller.getAllActiveProd().then(result =>
    res.send(result)
      )
    :
    res.send('Unauthorized');
  });

   // Retrieve All Inactive Products
  route.get('/inactive', (req, res) => {
    let token = req.headers.authorization;
    let isAdmin = auth.decode(token).isAdmin;
    isAdmin ? controller.getAllInactiveProd().then(result =>
    res.send(result)
      )
    :
    res.send('Unauthorized');
  });
  
  // Retrieve Single Product
  route.get('/:id', (req, res) => {
        let token = req.headers.authorization;
        let isAdmin = auth.decode(token).isAdmin;
        let id = req.params.id
        isAdmin ? controller.getProd(id).then(result => 
          res.send(result))
        : res.send(`User Unauthorized!`);
    });
  

// [SECTION] Route [PUT]
  // Update Product Information (Admin)
  route.put('/:prodId', auth.verify, (req, res) => {
    let token = req.headers.authorization;
    let isAdmin = auth.decode(token).isAdmin;
    let params = req.params;
    let body = req.body;
    isAdmin ? controller.updateProd(params, body).then(result => res.send(result))
    : res.send(`User Unauthorized!`);
  });

  // Product Archive (Admin)
  route.put('/:prodId/archive', auth.verify, (req, res) => {
    let token = req.headers.authorization;
    let isAdmin = auth.decode(token).isAdmin;
    let params = req.params

    isAdmin ? controller.archiveProd(params).then(result => res.send(result))
    : res.send(`User Unauthorized!`);
  });


// [SECTION] Expose Route System
  module.exports = route;





