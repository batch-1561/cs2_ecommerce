// [SECTION] Dependencies and Modules
	const exp = require('express');
	const controller = require('../controllers/users');
	const auth = require('../auth');

// [SECTION] Routing Component
	const route = exp.Router();

// [SECTION] Route - POST
	// Register User
	route.post('/register', (req, res) => {
		let userData = req.body;
		controller.registerUser(userData).then(outcome => {
			res.send(outcome);
		});
	});

// [SECTION] Route - POST
	// Change Password
	route.put('/upass', auth.verify, (req, res) => {
		let token = req.headers.authorization;
		let payload = auth.decode(token).email;
		let email = req.body.email;
		if (payload !== email) {
			res.send(`User Unauthorized to Proceed!`);
		} else {
			let data = req.body;
			controller.updatePass(data).then(outcome => {
				res.send(outcome);
			});
		}
	});



// [SECTION] Routes - [GET]
	// Retrieve All Users
	route.get('/all', (req, res) => {
		controller.getAllUsers().then(outcome => {
			res.send(outcome);
		});
	});

	// Retrieve Authenticated User's Orders
	route.get('/user-order', auth.verify,(req, res) => {
		let userData = auth.decode(req.headers.authorization);
		let userId = userData.id;
		controller.getUserOrd(userId).then(outcome => {
			res.send(outcome);
		});
	});

// [SECTION] Routes - [UPDATE]
	// Set user as admin with dynamic address
	route.put('/:userId/set-as-admin', auth.verify, (req, res) => {
		let token = req.headers.authorization
		let payload = auth.decode(token);
		let isAdmin = payload.isAdmin;
		let id = req.params.userId;
		(isAdmin) ? controller.setAdmin(id).then(outcome => res.send(outcome))
		: res.send(`Unauthorized User`);
	});

	route.put('/:userId/set-as-user', auth.verify, (req, res) => {
		let token = req.headers.authorization;
		let isAdmin = auth.decode(token).isAdmin;
		let id = req.params.userId;
		isAdmin ? 
		controller.setNonAdmin(id).then(result => res.send(result))
		: res.send(`Unauthorzied User!`);
	});

// [SECTION] Routes - [LOGIN]
	// Login User
	route.post('/login', (req, res) => {
		let data = req.body;
		controller.loginUser(data).then(outcome => {
			res.send(outcome);
		});
	});
		
// [SECTION] Expose Route System
	module.exports = route;
