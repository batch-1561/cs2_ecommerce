// [SECTION] Dependencies and Modules
	const exp = require('express');
	const controller = require('../controllers/orders');
	const auth = require('../auth');

// [SECTION] Routing Component
	const route = exp.Router();

// [SECTION] Route - POST
	// Create Order
	route.post('/order', auth.verify, (req, res) => {
		let token = req.headers.authorization;
		let payload = auth.decode(token);
		let userId = payload.id;
		let isAdmin = payload.isAdmin;
		let pId = req.body.prodId;
		let quantity = req.body.quantity;
		let data = {
			userId: userId,
			prodId: pId,
			quantity: quantity
		};

		if (!isAdmin) {
			controller.createOrder(data).then(outcome => {
				res.send(outcome);
			});
		} else {
			res.send(`Admministrator accounts are not allowed to order!`);
		}
	});

// [SECTION] Order [GET]
	// Retrieve All Orders
	route.get('/all', (req, res) => {
		controller.getAllOrders().then(result => {
			res.send(result);
		});
	});

 // [SECTION] Order [DEL]
  //[SECTION]-[DEL] Order Administrator Only
  route.delete('/:orderId/delete',auth.verify, (req, res) => {
     let token = req.headers.authorization;
     let isAdmin = auth.decode(token).isAdmin;
     let id = req.params.orderId;
     isAdmin ? controller.deleteOrder(id).then(outcome => res.send(outcome))
     :
     res.send('Unauthorized');
  });



// [SECTION] Order [POST]


// [SECTION] Order [GET]
	// Retrieve All Orders
	route.get('/checkout', (req, res) => {
		controller.checkAllOrders().then(result => {
			res.send(result);
		});
	});



// [SECTION] Expose Order System
	module.exports = route;
